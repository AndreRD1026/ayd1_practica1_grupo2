CREATE DATABASE  IF NOT EXISTS `hotel_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `hotel_db`;
-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: hotel_db
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_cliente`
--

DROP TABLE IF EXISTS `t_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_cliente` (
  `idt_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `idt_persona` int(11) NOT NULL,
  PRIMARY KEY (`idt_cliente`),
  KEY `persona_cliente` (`idt_persona`),
  CONSTRAINT `persona_cliente` FOREIGN KEY (`idt_persona`) REFERENCES `t_persona` (`idt_persona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='tabla de definicion de clientes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_cliente`
--

LOCK TABLES `t_cliente` WRITE;
/*!40000 ALTER TABLE `t_cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_habitacion`
--

DROP TABLE IF EXISTS `t_habitacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_habitacion` (
  `idt_habitacion` int(11) NOT NULL AUTO_INCREMENT,
  `numerohabitacion` int(11) NOT NULL COMMENT 'indica del piso y el numero de habitacion (21= piso 2 habitacion 1)',
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(200) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 0 COMMENT '0 = libre, 1 = ocupada, 2 = mantenimiento',
  PRIMARY KEY (`idt_habitacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='tabla de definicion de habitaciones';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_habitacion`
--

LOCK TABLES `t_habitacion` WRITE;
/*!40000 ALTER TABLE `t_habitacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_habitacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_persona`
--

DROP TABLE IF EXISTS `t_persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_persona` (
  `idt_persona` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) NOT NULL,
  `noidentificacion` int(15) NOT NULL,
  `numerotel` int(8) DEFAULT NULL,
  `idt_tipodocto` int(11) NOT NULL,
  PRIMARY KEY (`idt_persona`),
  KEY `docto_persona` (`idt_tipodocto`),
  CONSTRAINT `docto_persona` FOREIGN KEY (`idt_tipodocto`) REFERENCES `t_tipodoctoident` (`idt_tipodoctoident`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='tabla de definiciion de personas';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_persona`
--

LOCK TABLES `t_persona` WRITE;
/*!40000 ALTER TABLE `t_persona` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_reservacion`
--

DROP TABLE IF EXISTS `t_reservacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_reservacion` (
  `idt_reservacion` int(11) NOT NULL AUTO_INCREMENT,
  `idt_cliente` int(11) NOT NULL,
  `idt_habitacion` int(11) NOT NULL,
  `fecha_ini` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `estado_reservacion` int(11) NOT NULL DEFAULT 1 COMMENT '1 = creada, 2 = modificada, 3 = cancelada, 4 vencida',
  PRIMARY KEY (`idt_reservacion`),
  KEY `reserva_cliente` (`idt_cliente`),
  KEY `reserva_habitacion` (`idt_habitacion`),
  CONSTRAINT `reserva_cliente` FOREIGN KEY (`idt_cliente`) REFERENCES `t_cliente` (`idt_cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `reserva_habitacion` FOREIGN KEY (`idt_habitacion`) REFERENCES `t_habitacion` (`idt_habitacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='tabla de definicion de reservaciones hoteleras';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_reservacion`
--

LOCK TABLES `t_reservacion` WRITE;
/*!40000 ALTER TABLE `t_reservacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_reservacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_rol`
--

DROP TABLE IF EXISTS `t_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_rol` (
  `idt_rol` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  PRIMARY KEY (`idt_rol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='tabla de definicion de roles de usuario';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_rol`
--

LOCK TABLES `t_rol` WRITE;
/*!40000 ALTER TABLE `t_rol` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_tipodoctoident`
--

DROP TABLE IF EXISTS `t_tipodoctoident`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_tipodoctoident` (
  `idt_tipodoctoident` int(11) NOT NULL AUTO_INCREMENT,
  `descrpcion` varchar(50) NOT NULL,
  PRIMARY KEY (`idt_tipodoctoident`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='tabla de tipo de documento de intentificacion.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_tipodoctoident`
--

LOCK TABLES `t_tipodoctoident` WRITE;
/*!40000 ALTER TABLE `t_tipodoctoident` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_tipodoctoident` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_usuario`
--

DROP TABLE IF EXISTS `t_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_usuario` (
  `idt_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `idt_persona` int(11) NOT NULL,
  `idt_rol` int(11) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `contrasenia` varchar(100) NOT NULL,
  `estado` int(11) NOT NULL DEFAULT 1 COMMENT '0 = inactivo, 1 = activo',
  PRIMARY KEY (`idt_usuario`),
  KEY `rol_user` (`idt_rol`),
  KEY `persona_usuario` (`idt_persona`),
  CONSTRAINT `persona_usuario` FOREIGN KEY (`idt_persona`) REFERENCES `t_persona` (`idt_persona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `rol_user` FOREIGN KEY (`idt_rol`) REFERENCES `t_rol` (`idt_rol`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci COMMENT='tabla de definicion de usuarios';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_usuario`
--

LOCK TABLES `t_usuario` WRITE;
/*!40000 ALTER TABLE `t_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-18  0:29:47
