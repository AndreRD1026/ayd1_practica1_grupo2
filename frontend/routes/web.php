<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginRegisterController;
use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\AdminClientsController;
use App\Http\Controllers\ClientDashboardController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('index');
});



Route::controller(LoginRegisterController::class) ->group(function() { 
    Route::get('/register', 'register')->name('register');
    Route::get('/login', 'login')->name('login');
    Route::get('/dashboard', 'dashboard')->name('dashboard');
    Route::post('/autenticar', 'autenticar')->name('autenticar');
    Route::post('/store', 'store')->name('store');
});

Route::controller(AdminDashboardController::class) ->group(function() { 
    Route::get('/admin/dashboard', 'index')->name('admin.dashboard');
    Route::post('/rooms', 'store')->name('rooms.store');
    Route::get('/rooms/{id}/edit', 'edit')->name('rooms.edit');
    Route::put('/rooms/{id}', 'update')->name('rooms.update');
    Route::delete('/rooms/{id}', 'delete')->name('rooms.delete');
});

Route::controller(AdminClientsController::class) ->group(function() { 
    Route::get('/admin/reservation', 'index')->name('admin.reservation');
    Route::post('/reservation', 'store')->name('reservation.store');
    Route::get('/reservation/{id}/edit', 'edit')->name('reservation.edit');
    Route::put('/reservation/{id}', 'update')->name('reservation.update');
    
});

Route::controller(ClientDashboardController::class) ->group(function() { 
    Route::get('/clients/reservations', 'index')->name('homeclients');
    Route::get('/clients/reservations/create', 'crear_reservacion')->name('reservaciones.create');
    Route::post('/clients/reservations/create','store')->name('reservaciones.store');
    Route::delete('/clients/reservations/{id}', 'delete')->name('reservaciones.delete');
    Route::get('/clients/edit/{id}', 'edit')->name('reservaciones.edit');
    Route::put('/clients/edit/{id}', 'update')->name('reservaciones.update');

});