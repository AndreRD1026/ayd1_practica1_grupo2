<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ClientDashboardController extends Controller
{
    public function index()
    {
        $data = $this->getReservations();
        return view('user.index', compact('data'));
    }

    public function getReservations()
    {
        $response = Http::get('http://localhost:3002/reservaciones/allreservation');
        if ($response->successful()) {
            $reservations = $response->json();

            return $reservations;
        }

        return [];
    }

    public function crear_reservacion()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
        $response = Http::post('http://localhost:3002/reservaciones/add', [
            'idt_habitacion' =>  intval($request->habitacion),
            'fecha_entrada' => $request->fecha_entrada,
            'fecha_salida' => $request->fecha_salida,
        ]);
    return redirect()->route('homeclients');
    }

    public function edit($id)
    {
        $response = Http::get("http://localhost:3002/reservaciones/{$id}");
        if ($response->successful()) {
            $reservation = $response->json();

            return view('user.editreservacion', compact('reservation'));
        }

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $response = Http::put("http://localhost:3002/reservaciones/reservacioneseditada/{$id}", [
            'idt_habitacion' => intval($request->idt_habitacion),
        ]);

        if ($response->successful()) {
            return redirect()->route('homeclients');
        }

        return redirect()->back();
    }

    public function delete($id)
    {
        $response = Http::delete("http://localhost:3002/reservaciones/reservacionescliente/{$id}");
        if ($response->successful()) {
            return redirect()->route('homeclients');
        }
        return redirect()->back();
    }
}
