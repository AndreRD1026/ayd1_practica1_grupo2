<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class AdminDashboardController extends Controller
{
    // Método para mostrar el dashboard del administrador
    public function index()
    {
        $rooms = $this->getRooms();
        return view('admin.dashboard.index', compact('rooms'));
    }

    
    public function getRooms()
    {
        $response = Http::get('http://localhost:3002/rooms');
        if ($response->successful()) {
            $rooms = $response->json();

            return $rooms;
        }

        return [];
    }

    public function store(Request $request)
    {
        $response = Http::post('http://localhost:3002/rooms', [
            'numerohabitacion' => intval($request->numerohabitacion),
            'nombre' => $request->nombre,
            'descripcion' => $request->descripcion,
            'estado' => intval($request->estado)
        ]);

        if ($response->successful()) {
            return redirect()->route('admin.dashboard');
        }

        return redirect()->back();
    }

    public function edit($id)
    {
        $response = Http::get("http://localhost:3002/rooms/{$id}");
        if ($response->successful()) {
            $room = $response->json();

            return view('admin.dashboard.edit', compact('room'));
        }

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $response = Http::put("http://localhost:3002/rooms/{$id}", [
            'numerohabitacion' => intval($request->numerohabitacion),
            'nombre' => $request->nombre,
            'descripcion' => $request->descripcion,
            'estado' => intval($request->estado)
        ]);

        if ($response->successful()) {
            return redirect()->route('admin.dashboard');
        }

        return redirect()->back();
    }

    public function delete($id)
    {
        $response = Http::delete("http://localhost:3002/rooms/{$id}");
        if ($response->successful()) {
            return redirect()->route('admin.dashboard');
        }

        return redirect()->back();
    }
}
