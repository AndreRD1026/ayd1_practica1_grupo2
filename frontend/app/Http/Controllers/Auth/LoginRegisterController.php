<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Middleware\EncryptCookies;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Http;


class LoginRegisterController extends Controller
{
    //
    public function __construct(){
        $this->middleware('guest')->except([
            'logout', 'dashboard'
        ]);
    }

    public function register(){
        return view('auth.register');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|string|max:250',
            'user' => 'required|string|max:250',
            'password' => 'required|min:8|confirmed',
            'tipoident' => 'required|numeric',
            'ident' => 'required|numeric',
            'tel' => 'required|numeric',
        ]);
        error_log('llegamos aqui');
        $hostname = env("SERVER_BACKEND", "localhost:3002");
        error_log($hostname.'/user/register');
        $response = Http::post($hostname.'/user/register', [
            'nombres' => $request->name,
            'usuario' => $request->email,
            'contrasena' => $request-> password,
            'numero_tel' => $request->tipoident,
            'no_identificacion' => $request->ident,
            'descripcion_doc' => $request-> tel,
            'descripcion_rol' => "user",
            'estado' => 1
        ]);
        error_log('Some message here.');
        Cookie::queue(Cookie::make('username', $response));

        /**
         * $hostname = env("SERVER_BACKEND", "localhost:3002");
         * Http::withUrlParameters([
            *'endpoint' =>  $hostname,
            *'user' => 'admin',
            *'pass' => 'admin',
            *])->get('{+endpoint}/{user}/{pass}'); 
            */
        $request->session()->regenerate();
        return redirect()->route('dashboard')->withSuccess('Registrado y logeado');
    }


    public function login(){
        return view('auth.login');
    }

    public function autenticar(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);
        // consumimos api de go para validar usuario

        /**
         * $hostname = env("SERVER_BACKEND", "localhost:3002");
         * Http::withUrlParameters([
            *'endpoint' =>  $hostname,
            *'user' => 'admin',
            *'pass' => 'admin',
            *])->get('{+endpoint}/{user}/{pass}'); 
            */

        if($request->email == 'admin@admin.com' && $request->password == 'admin'){
            $request->Session()->put('admin', true);
            $request->Session()->put('username', 'ADMIN');
            Cookie::queue(Cookie::make('admin', true));
            Cookie::queue(Cookie::make('login', true));
            Cookie::queue(Cookie::make('username', 'Admin'));

            return redirect()->route('dashboard')
                ->withSuccess('You have successfully logged in!');
        } else if ($request->email == 'user@user.com' && $request->password == 'user'){
            $request->Session()->put('admin', false);
            $request->Session()->put('username', 'USER');
            Cookie::queue(Cookie::make('login', true));
            Cookie::queue(Cookie::make('admin', false));
            Cookie::queue(Cookie::make('username', 'USER'));
            

            return redirect()->route('dashboard')
                ->withSuccess('You have successfully logged in!');
        }

        return back()->withErrors([
            'email' => 'Your provided credentials do not match in our records.',
        ])->onlyInput('email');

    } 

    public function dashboard()
    {
        if(Cookie::get('login')) /*aqui validamos si es admin o es cliente y se redirige*/
        {
            return view('auth.dashboardtest');
        }
        
        return redirect()->route('login')
            ->withErrors([
            'email' => 'Please login to access the dashboard.',
        ])->onlyInput('email');
    } 

}
