<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class AdminClientsController extends Controller
{
    public function index()
    {
        $reservation = $this->getReservation();
        return view('admin.clients.index', compact('reservation'));
    }

    public function getReservation()
    {
        $response = Http::get('http://localhost:3002/reservaciones');
        if ($response->successful()) {
            $reservation = $response->json();

            return $reservation;
        }

        return [];
    }

    /*	type Reservacion struct {
		IDT_Reservacion    int    `json:"idt_reservacion"`
		IDT_Cliente        int    `json:"idt_cliente"`
		IDT_Habitacion     int    `json:"idt_habitacion"`
		Fecha_Ini          string `json:"fecha_ini"`
		Fecha_Fin          string `json:"fecha_fin"`
		Estado_Reservacion int    `json:"estado_reservacion"`
	*/

    public function store(Request $request)
    {
        $response = Http::post('http://localhost:3002/reservaciones', [
            'idt_cliente' => intval($request->idt_cliente),
            'idt_habitacion' => intval($request->idt_habitacion),
            'fecha_ini' => $request->fecha_ini,
            'fecha_fin' => $request->fecha_fin,
            'estado_reservacion' => intval($request->estado_reservacion)
        ]);

        if ($response->successful()) {
            return redirect()->route('admin.reservation');
        }

        return redirect()->back();

    }

    public function edit($id)
    {
        $response = Http::get("http://localhost:3002/reservaciones/{$id}");
        if ($response->successful()) {
            $reservation = $response->json();

            return view('admin.clients.edit', compact('reservation'));
        }

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $response = Http::put("http://localhost:3002/reservaciones/{$id}", [
            'idt_habitacion' => intval($request->idt_habitacion),
        ]);

        if ($response->successful()) {
            return redirect()->route('admin.reservation');
        }

        return redirect()->back();
    }

    public function delete($id)
    {
        $response = Http::delete("http://localhost:3002/reservaciones/{$id}");
        if ($response->successful()) {
            return redirect()->route('admin.reservation');
        }

        return redirect()->back();
    }


}
