@extends('index')

@section('content')

<div class="container">
    <div class="columns">
    <div class="column is-half is-offset-one-quarter">

        <div class="card">
            <div class="card-header has-background-info"><h1 class="card-header-title">Login</h1></div>
            <div class="card-content">
                <form action="{{ route('autenticar') }}" method="post">
                    @csrf
                    <div class="field">
                        <label for="email" class="label">Correo</label>
                        <div class="control">
                          <input type="email" class="input @error('email') is-danger @enderror" id="email" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="help is-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="field">
                        <label for="password" class="label">Contraseña</label>
                        <div class="control">
                          <input type="password" class="input @error('password') is-danger @enderror" id="password" name="password">
                            @if ($errors->has('password'))
                                <span class="help is-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="field">
                        <input type="submit" class="button is-success" value="Login">
                    </div>
                    
                </form>
            </div>
        </div>
    </div>    
</div>
    
@endsection