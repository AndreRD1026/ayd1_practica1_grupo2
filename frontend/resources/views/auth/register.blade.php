@extends('index')

@section('content')

<div class="container">
    <div class="columns">
    <div class="column is-half is-offset-one-quarter">
        <div class="card ">
            <div class="card-header has-background-info  "><h1 class="card-header-title">Sign-up</h1></div>
            <div class="card-content">
                <form action="{{ route('store') }}" method="post">
                    @csrf
                    <div class="field">
                        <label for="name" class="label">Nombre</label>
                        <div class="control">
                          <input type="text" class="input @error('name') is-danger @enderror" id="name" name="name" value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="help is-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="field">
                        <label for="user" class="label">Usuario</label>
                        <div class="control">
                          <input type="text" class="input @error('user') is-danger @enderror" id="user" name="user" value="{{ old('user') }}">
                          @if ($errors->has('user'))
                                <span class="help is-danger">{{ $errors->first('user') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="field">
                        <label for="tipoident" class="label">Tipo Identificacion</label>
                        <div class="control">
                          <input type="text" class="input @error('tipoident') is-danger @enderror" id="tipoident" name="tipoident" value="{{ old('tipoident') }}">
                          @if ($errors->has('tipoident'))
                                <span class="help is-danger">{{ $errors->first('tipoident') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="field">
                        <label for="ident" class="label">No. Identificacion</label>
                        <div class="control">
                          <input type="text" class="input @error('ident') is-danger @enderror" id="ident" name="ident" value="{{ old('ident') }}">
                          @if ($errors->has('ident'))
                                <span class="help is-danger">{{ $errors->first('ident') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="field">
                        <label for="tel" class="label">Telefono</label>
                        <div class="control">
                          <input type="tel" class="input @error('tel') is-danger @enderror" id="tel" name="tel" value="{{ old('tel') }}">
                          @if ($errors->has('tel'))
                                <span class="help is-danger">{{ $errors->first('tel') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="field">
                        <label for="password" class="label">Contraseña</label>
                        <div class="control">
                          <input type="password" class="input @error('password') is-danger @enderror" id="password" name="password">
                            @if ($errors->has('password'))
                                <span class="help is-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="field">
                        <label for="password_confirmation" class="label">Confirma tu Contraseña</label>
                        <div class="control">
                          <input type="password" class="input" id="password_confirmation" name="password_confirmation">
                        </div>
                    </div>
                    <div class="field">
                        <input type="submit" class="button is-success" value="Registrar">
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
    </div>
</div>
    
@endsection