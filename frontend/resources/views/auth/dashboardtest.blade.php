@extends('index')

@section('content')

<div class="container">
    <div class="columns">
        <div class="card">
            <div class="card-header">Dashboard</div>
            <div class="card-content">
                @if (session('admin'))
                    <div class="">
                       Es admin {{ Cookie::get('username') }}
                    </div>
                @else
                    <div class="">
                        es usuario {{session('username')}}
                    </div>       
                @endif                
            </div>
        </div>
    </div>    
</div>
    
@endsection