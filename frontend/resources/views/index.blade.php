<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>AyD1-Grupo2</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Styles -->
       
    </head>
    <body>
        
        <navbar class="navbar is-dark" role="navigation">
        <div class="navbar-menu">
            <div class="navbar-start" >
            <a class="navbar-item" href="{{ URL('/') }}">Inicio</a>

            @if (Cookie::get('admin'))
                <a class="navbar-item" href="{{ route('admin.dashboard') }}">
                    Habitaciones
                </a>
                <a class="navbar-item" href="{{ route('admin.reservation') }}">
                    Reservaciones
                </a>
            @elseif (Cookie::get('admin') == false && Cookie::get('login'))
                <a class="navbar-item" href="{{ route('homeclients') }}">
                    Ver Reservaciones
                </a>
            @endif
          
            </div>
          </div>

        <div class="navbar-end">
            @if (!Cookie::get('login'))
                <div class="navbar-item">
                    <div class="buttons">
                            <a class="button is-primary {{ (request()->is('login')) ? 'is-active' : ''}}" href="{{ route('register') }}" >
                                <strong>Sign up</strong>
                            </a>
                            <a class="button is-light {{ (request()->is('register')) ? 'active' : '' }}" href="{{ route('login') }}">
                                Log in
                            </a>
                    </div>
                </div>
            @else
            <a class="button is-light {{ (request()->is('register')) ? 'active' : '' }}" class="{{ Cookie::queue(Cookie::make('admin', false));}}" href="{{ route('login') }}">
                                Log Out
                            </a>
                <a class="navbar-item">{{ Cookie::get('username') }} </a>
                <!-- agregar mas funciones de usr -->

            @endguest
        </div>
        
        </navbar>

            <div class="section">
                @yield('content')
            </div>
        

        <footer class="content has-text-centered">
                        Algun texto en el footer
        </footer>
    </body>
</html>
