

@extends('index')

@section('content')
    <h1 class="title" >Editar Habitación</h1>

    <form action="/rooms/{{ $room['idt_habitacion'] }}" method="POST" class="box">
        @csrf
        @method('PUT')

        <div class="field">
            <label for="idt_habitacion" class="label">ID de Habitación:</label>
            <div class="control">
                <input type="text" id="idt_habitacion" style="font-weight: bold;" name="idt_habitacion" value="{{ $room['idt_habitacion'] }}" class="input" readonly>
            </div>
        </div>

        <div class="field">
            <label for="numerohabitacion" class="label">Número de Habitación:</label>
            <div class="control">
                <input type="text" id="numerohabitacion" name="numerohabitacion" value="{{ $room['numerohabitacion'] }}" class="input" required>
            </div>
        </div>

        <div class="field">
            <label for="nombre" class="label">Nombre:</label>
            <div class="control">
                <input type="text" id="nombre" name="nombre" value="{{ $room['nombre'] }}" class="input" required>
            </div>
        </div>

        <div class="field">
            <label for="descripcion" class="label">Descripción:</label>
            <div class="control">
                <input type="text" id="descripcion" name="descripcion" value="{{ $room['descripcion'] }}" class="input" required>
            </div>
        </div>

        <div class="field">
            <label for="estado" class="label">Estado:</label>
            <div class="control">
                <input type="number" id="estado" name="estado" value="{{ $room['estado'] }}" class="input" required>
            </div>
        </div>

        <div class="field">
            <div class="control">
                <button type="submit" class="button is-link">Update</button>
            </div>
        </div>
    </form>

@endsection