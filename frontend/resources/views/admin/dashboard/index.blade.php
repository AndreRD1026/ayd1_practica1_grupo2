
@extends('index')

@section('content')

<h1 class="title">Crear Habitación</h1>

<form action="/rooms" method="POST" class="form">
    @csrf
    <div class="field">
        <label for="numerohabitacion" class="label">Número de Habitación:</label>
        <div class="control">
            <input type="text" id="numerohabitacion" name="numerohabitacion" class="input" required>
        </div>
    </div>

    <div>
        <label for="nombre" class="label">Nombre:</label>
        <div class="control">
            <input type="text" id="nombre" name="nombre" class="input" required>
        </div>
    </div>

    <div class="field">
        <label for="descripcion" class="label">Descripción:</label>
        <div class="control">
            <input type="text" id="descripcion" name="descripcion" class="input" required>
        </div>
    </div>

    <div class="field">
        <label for="estado" class="label">Estado:</label>
        <div class="control">
            <input type="number" id="estado" name="estado" class="input" required>
        </div>
    </div>




    <div class="field">
        <div class="control">
            <button type="submit" class="button is-link">Create</button>
        </div>
    </div>
</form>

<br><br>

<table class="table is-fullwidth">
    <thead>
        <tr>
            <th>ID</th>
            <th>Número de Habitación</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @if ($rooms === null || count($rooms) === 0)
        <tr>
            <td colspan="4">No rooms found.</td>
        </tr>
        @elseif (count($rooms) > 0)
            @foreach ($rooms as $room)
            <tr>
                <td>{{ $room['idt_habitacion'] }}</td>
                <td>{{ $room['numerohabitacion'] }}</td>
                <td>{{ $room['nombre'] }}</td>
                <td>{{ $room['descripcion'] }}</td>
                <td>{{ $room['estado'] }}</td>
                <td>
                    <a class="button is-info" href="/rooms/{{ $room['idt_habitacion'] }}/edit">Edit</a>
                    <form action="/rooms/{{ $room['idt_habitacion'] }}" method="POST" style="display: inline;">
                        @csrf
                        @method('DELETE')
                        <button class="button is-danger" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        @endif
    </tbody>
</table>
@endsection
