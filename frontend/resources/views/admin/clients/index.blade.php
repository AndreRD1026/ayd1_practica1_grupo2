
@extends('index')

@section('content')

<h1 class="title">Crear Reservacion</h1>

<form action="/reservation" method="POST" class="form">
    @csrf
    <div class="field">
        <label for="idt_cliente" class="label">ID de Cliente:</label>
        <div class="control">
            <input type="text" id="idt_cliente" name="idt_cliente" class="input" required>
        </div>
    </div>

    <div>
        <label for="idt_habitacion" class="label">ID de Habitación:</label>
        <div class="control">
            <input type="text" id="idt_habitacion" name="idt_habitacion" class="input" required>
        </div>
    </div>

    <div class="field">
        <label for="fecha_ini" class="label">Fecha de Inicio:</label>
        <div class="control">
            <input type="date" id="fecha_ini" name="fecha_ini" class="input" required>
        </div>
    </div>

    <div class="field">
        <label for="fecha_fin" class="label">Fecha de Fin:</label>
        <div class="control">
            <input type="date" id="fecha_fin" name="fecha_fin" class="input" required>
        </div>
    </div>

    <div class="field">
        <label for="estado_reservacion" class="label">Estado de Reservacion:</label>
        <div class="control">
            <input type="number" id="estado_reservacion" name="estado_reservacion" class="input" required>
        </div>
    </div>

    <div class="field">
        <div class="control">
            <button type="submit" class="button is-link">Create</button>
        </div>
    </div>
    
</form>

    <br><br>

    <table class="table is-fullwidth">
        <thead>
            <tr>
                <th>ID</th>
                <th>ID de Cliente</th>
                <th>ID de Habitación</th>
                <th>Fecha de Inicio</th>
                <th>Fecha de Fin</th>
                <th>Estado de Reservacion</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($reservation as $reservation)
            <tr>
                <td>{{ $reservation['idt_reservacion'] }}</td>
                <td>{{ $reservation['idt_cliente'] }}</td>
                <td>{{ $reservation['idt_habitacion'] }}</td>
                <td>{{ $reservation['fecha_ini'] }}</td>
                <td>{{ $reservation['fecha_fin'] }}</td>
                <td>{{ $reservation['estado_reservacion'] }}</td>
                <td>
                    <a href="/reservation/{{ $reservation['idt_reservacion'] }}/edit" class="button is-link">Edit</a>
                </td>
            </tr>
            @endforeach
        </tbody>

    </table>

@endsection


