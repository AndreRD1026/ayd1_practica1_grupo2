@extends('index')

@section('content')

<!DOCTYPE html>
<html>
<head>
    <title>Reservaciones</title>
    <link rel="stylesheet" href="{{ asset('css/reservaciones.css') }}">
</head>
<body>

<button class="button is-link" onclick="window.location='{{ route('reservaciones.create') }}'">Crear Reservación</button>

<h2>Reservaciones Pendientes</h2>

<!-- Contenedor para las reservaciones -->
<div id="contenedorReservaciones">
    @if ($data === null || count($data) === 0)
        <p>No hay reservaciones</p>
    @else 
    @foreach($data as $reservacion)
    <div class="reservacion-info">
        <h3>Datos de la Reservación</h3>
        <div>
            <input type="hidden" id="id_reservacion_{{ $reservacion['idt_reservacion'] }}" value="{{ $reservacion['idt_reservacion'] }}">
        </div>
        <div>
            <strong>Habitación:</strong> <span>{{ $reservacion['idt_habitacion']}}</span>
        </div>
        <div>
            <strong>Fecha de Entrada:</strong> <span>{{ $reservacion['fecha_entrada']}}</span>
        </div>
        <div>
            <strong>Fecha de Salida:</strong> <span>{{ $reservacion['fecha_salida']}}</span>
        </div>

        <div>
            <strong>Estado:</strong>
            @php
                switch ($reservacion['estado_reservacion']) {
                    case 1:
                        echo "Activa";
                        break;
                    case 2:
                        echo "Modificada";
                        break;
                    case 3:
                        echo "Cancelada";
                        break;
                    case 4:
                        echo "Vencida";
                        break;
                    default:
                        echo "Desconocido";
                }
            @endphp
        </div>

        <div>
            <!-- Botón de editar -->
            <a href="/clients/edit/{{ $reservacion['idt_reservacion'] }}" class="button is-link">Editar</a>

            <!-- Botón de eliminar -->
            <form id="form-eliminar-{{ $reservacion['idt_reservacion'] }}" action="/clients/reservations/{{ $reservacion['idt_reservacion'] }}" method="POST" style="display: inline;">
                @csrf
                @method('DELETE')
                <button class="button is-danger" type="button" onclick="confirmarEliminacion({{ $reservacion['idt_reservacion'] }})">Delete</button>
            </form>

        </div>
    </div>
    @endforeach
    @endif
</div>

<script>
    function confirmarEliminacion(id) {
        if (confirm('¿Está seguro de que desea eliminar esta reservación?')) {
            document.getElementById('form-eliminar-' + id).submit();
        }
    }
</script>

</body>
</html>

@endsection