@extends('index')

@section('content')

<h1 class="title">Editar Reservacion</h1>

<form action="/clients/edit/{{ $reservation['idt_reservacion'] }}" method="POST" class="box">

    @csrf
    @method('PUT')

    <div class="field">
        <label for="idt_reservacion" class="label">ID de Reservacion:</label>
        <div class="control">
            <input type="text" id="idt_reservacion" style="font-weight: bold;" name="idt_reservacion" value="{{ $reservation['idt_reservacion'] }}" class="input" readonly>
        </div>
    </div>

    <div class="field">
        <label for="idt_cliente" class="label">ID de Cliente:</label>
        <div class="control">
            <input type="text" id="idt_cliente" style="font-weight: bold;" name="idt_cliente" value="{{ $reservation['idt_cliente'] }}" class="input" readonly>
        </div>
    </div>

    <div>
        <label for="idt_habitacion" class="label">ID de Habitación:</label>
        <div class="control">
            <input type="text" id="idt_habitacion" name="idt_habitacion" value="{{ $reservation['idt_habitacion'] }}" class="input" required>
        </div>
    </div>

    <div class="field">
        <label for="fecha_ini" class="label">Fecha de Inicio:</label>
        <div class="control">
            <input type="date" id="fecha_ini" style="font-weight: bold;" name="fecha_ini" value="{{ $reservation['fecha_entrada'] }}" class="input" readonly>
        </div>
    </div>

    <div class="field">
        <label for="fecha_fin" class="label">Fecha de Fin:</label>
        <div class="control">
            <input type="date" id="fecha_fin" style="font-weight: bold;" name="fecha_fin" value="{{ $reservation['fecha_salida'] }}" class="input" readonly>
        </div>
    </div>

    <div class="field">
        <label for="estado_reservacion" class="label">Estado de Reservacion:</label>
        <div class="control">
            <input type="number" id="estado_reservacion" style="font-weight: bold;" name="estado_reservacion" value="{{ $reservation['estado_reservacion'] }}" class="input" readonly>
        </div>
    </div>

    <div class="field">
        <div class="control">
            <button type="submit" class="button is-link">Update</button>
        </div>
    </div>

</form>

@endsection