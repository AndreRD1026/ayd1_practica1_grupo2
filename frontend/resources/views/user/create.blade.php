@extends('index')

@section('content')

<!DOCTYPE html>
<html>
<head>
    <title>Reservaciones</title>
    <link rel="stylesheet" href="{{ asset('css/reservaciones.css') }}">
</head>
<body>
<!-- Botón para crear una nueva reserva -->
<button class="button is-link" onclick="mostrarFormulario()">Nueva Reservación</button>

<!-- Contenedor para los datos de la reserva (inicialmente oculto) -->
<div id="datosReservacion" style="display: none;">
    <div class="reservacion-info">
        <h3>Datos de la Reservación</h3>
        <div>
            <strong>Habitación:</strong> <span id="habitacionSeleccionada"></span>
        </div>
        <div>
            <strong>Fecha de Entrada:</strong> <span id="fechaEntrada"></span>
        </div>
        <div>
            <strong>Fecha de Salida:</strong> <span id="fechaSalida"></span>
        </div>
    </div>
</div>

<!-- Formulario para crear una nueva reserva (inicialmente oculto) -->
<div id="formularioReservacion" style="display: none;">
    <h3>Nueva Reservación</h3>
    <form action="{{route('reservaciones.store')}}" method="POST">
        @csrf
        <div>
            <label for="habitacion">Habitación:</label>
            <select id="habitacion" name="habitacion">
                <!-- Opciones para seleccionar la habitación -->
                <option value="1">Habitación 1</option>
                <option value="2">Habitación 2</option>
                <option value="3">Habitación 3</option>
                <option value="3">Habitación 4</option>
                <!-- Agrega más opciones según sea necesario -->
            </select>
        </div>
        <div>
            <label for="fecha_entrada">Fecha de Entrada:</label>
            <input type="date" id="fecha_entrada" name="fecha_entrada">
        </div>
        <div>
            <label for="fecha_salida">Fecha de Salida:</label>
            <input type="date" id="fecha_salida" name="fecha_salida">
        </div>
        <button type="submit">Crear Reservación</button>
    </form>
</div>

<script>
    // Función para mostrar el formulario de creación de reservaciones
    function mostrarFormulario() {
        var formulario = document.getElementById('formularioReservacion');
        formulario.style.display = 'block';
    }

</script>

</body>
</html>

@endsection