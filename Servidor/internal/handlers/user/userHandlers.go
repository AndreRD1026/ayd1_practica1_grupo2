package userHandlers

import (
	"fmt"
	"log"
	"main/database"
	"main/internal/models"

	"github.com/gofiber/fiber/v2"
)

func RegisterUser(c *fiber.Ctx) error {
	u := new(models.UserDTO)
	// Parse body into struct
	if err := c.BodyParser(u); err != nil {
		return c.Status(404).
			JSON(fiber.Map{"status": "error", "message": err.Error(), "data": nil})
	}
	// Insert documento into database
	res, err := database.DB.Query("INSERT INTO t_tipodoctoident (descrpcion) VALUES (?)", u.Descripcion_doc)
	if err != nil {
		return c.Status(404).
			JSON(fiber.Map{"status": "error", "message": err.Error(), "data": nil})
	}
	//Insert -> rol
	res, err = database.DB.Query("INSERT INTO t_rol (descripcion) VALUES (?)", u.Descripcion_rol)
	if err != nil {
		return c.Status(404).
			JSON(fiber.Map{"status": "error", "message": err.Error(), "data": nil})
	}
	//Insert -> persona
	res, err = database.DB.Query("INSERT INTO t_persona (nombres,noidentificacion,numerotel,idt_tipodocto) VALUES (?,?,?,(SELECT idt_tipodoctoident FROM t_tipodoctoident WHERE descrpcion=\""+fmt.Sprintf("%s", u.Descripcion_doc)+"\"))", u.Nombres, u.No_identificacion, u.Numero_tel)
	if err != nil {
		return c.Status(404).
			JSON(fiber.Map{"status": "error", "message": err.Error(), "data": nil})
	}

	res, err = database.DB.Query("INSERT INTO t_cliente (idt_persona) VALUES (select * from t_persona ORDER BY idt_persona DESC LIMIT 1)")
	if err != nil {
		return c.Status(404).
			JSON(fiber.Map{"status": "error", "message": err.Error(), "data": nil})
	}

	// Print result
	log.Println(res)
	// Return User in JSON format
	return c.JSON(u)
}
func UpdateUser(c *fiber.Ctx) error {
	// New user struct
	u := new(models.UserDTO)

	// Parse body into struct
	if err := c.BodyParser(u); err != nil {
		return c.Status(404).
			JSON(fiber.Map{"status": "error", "message": err.Error(), "data": nil})
	}

	// Update t_usuario record in database
	res, err := database.DB.Query("UPDATE t_usuario SET usuario=?,contrasenia=?,estado=? WHERE idt_usuario=?", u.Usuario, u.Contrasena, u.Estado, u.Id_usuario)
	if err != nil {
		return c.Status(500).
			JSON(fiber.Map{"status": "error", "message": err.Error(), "data": nil})
	}

	// Update t-persona record in database
	res, err = database.DB.Query("UPDATE t_persona SET nombres=?,noidentificacion=?,numerotel=?,idt_tipodocto=? WHERE idt_usuario=?", u.Nombres, u.No_identificacion, u.Numero_tel, u.Id_documento)
	if err != nil {
		return c.Status(500).
			JSON(fiber.Map{"status": "error", "message": err.Error(), "data": nil})
	}

	// Print result
	log.Println(res)

	// Return Employee in JSON format
	return c.Status(201).JSON(u)
}

// Delete record from MySQL
// func DeleteUser(c *fiber.Ctx) error {
// 	// New Employee struct
// 	u := new(models.UserDTO)

// 	// Parse body into struct
// 	if err := c.BodyParser(u); err != nil {
// 		return c.Status(404).
// 			JSON(fiber.Map{"status": "error", "message": err.Error(), "data": nil})
// 	}

// 	// Delete Employee from database
// 	res, err := database.DB.Query("DELETE FROM  WHERE id = ?", u.ID)
// 	if err != nil {
// 		return err
// 	}

// 	// Print result
// 	log.Println(res)

// 	// Return Employee in JSON format
// 	return c.JSON("Deleted")
// }
