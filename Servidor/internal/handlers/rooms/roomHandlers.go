package roomsHandlers

import (
	"main/internal/models"

	"github.com/gofiber/fiber/v2"
)

var rooms = []models.RoomDTO{
	{IDT_Habitacion: 1, NumeroHabitacion: 101, Nombre: "Habitacion 101", Descripcion: "Esta es la habitacion 101", Estado: 0},
	{IDT_Habitacion: 2, NumeroHabitacion: 102, Nombre: "Habitacion 102", Descripcion: "Esta es la habitacion 102", Estado: 0},
}

var reservaciones_cliente = []models.Reservacion_ClienteDTO{
	{IDT_Reservacion: 1, IDT_Cliente: 1, IDT_Habitacion: 1, Fecha_Ini: "2021-10-01", Fecha_Fin: "2021-10-05", Estado_Reservacion: 1},
	{IDT_Reservacion: 2, IDT_Cliente: 2, IDT_Habitacion: 2, Fecha_Ini: "2021-10-01", Fecha_Fin: "2021-10-05", Estado_Reservacion: 1},
}

func GetRooms(c *fiber.Ctx) error {
	return c.JSON(rooms)
}

func GetRoomsReservadas(c *fiber.Ctx) error {
	return c.JSON(reservaciones_cliente)
}

func AddRoom(c *fiber.Ctx) error {
	room := new(models.RoomDTO)
	if err := c.BodyParser(room); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": "Cannot parse JSON",
		})
	}
	room.IDT_Habitacion = len(rooms) + 1
	rooms = append(rooms, *room)
	return c.JSON(room)
}

func AddReservation(c *fiber.Ctx) error {
	reservation := new(models.Reservacion_ClienteDTO)
	if err := c.BodyParser(reservation); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": "Cannot parse JSON",
		})
	}
	reservation.IDT_Reservacion = len(reservaciones_cliente) + 1
	reservation.IDT_Cliente = 1
	reservation.Estado_Reservacion = 1
	reservaciones_cliente = append(reservaciones_cliente, *reservation)
	return c.JSON(reservation)
}
