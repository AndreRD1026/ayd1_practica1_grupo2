package reservacionesHandlers

import (
	"fmt"
	"main/internal/models"
	"strconv"

	"github.com/gofiber/fiber/v2"
)

var reservaciones = []models.ReservacionDTO{}
var reservaciones_cliente = []models.Reservacion_ClienteDTO{
	{IDT_Reservacion: 1, IDT_Cliente: 1, IDT_Habitacion: 1, Fecha_Ini: "2021-10-01", Fecha_Fin: "2021-10-05", Estado_Reservacion: 1},
	{IDT_Reservacion: 2, IDT_Cliente: 2, IDT_Habitacion: 2, Fecha_Ini: "2021-10-01", Fecha_Fin: "2021-10-05", Estado_Reservacion: 1},
}

func GetReservaciones(c *fiber.Ctx) error {
	return c.JSON(reservaciones_cliente)
}

func AddReservacion(c *fiber.Ctx) error {
	reservacion := new(models.Reservacion_ClienteDTO)
	if err := c.BodyParser(reservaciones_cliente); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"error": "Cannot parse JSON",
		})
	}
	reservacion.IDT_Reservacion = len(reservaciones) + 1
	reservaciones_cliente = append(reservaciones_cliente, *reservacion)
	return c.JSON(reservacion)
}

func GetOneReservacion(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))
	for _, reservacion := range reservaciones {
		if reservacion.IDT_Reservacion == id {
			return c.JSON(reservacion)
		}
	}
	return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
		"error": "Reservacion not found",
	})
}

func GetOneReservacionByCliente(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))
	for _, reservacion := range reservaciones_cliente {
		if reservacion.IDT_Reservacion == id {
			return c.JSON(reservacion)
		}
	}
	return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
		"error": "Reservacion not found",
	})
}

func UpdateReservacionEditAdmin(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))
	for i, reservacion := range reservaciones {
		if reservacion.IDT_Reservacion == id {
			reservacion := new(models.ReservacionDTO)
			if err := c.BodyParser(reservacion); err != nil {
				return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
					"error": "Cannot parse JSON",
				})
			}
			reservacion.IDT_Reservacion = id
			reservaciones[i].IDT_Habitacion = reservacion.IDT_Habitacion
			return c.JSON(reservacion)
		}
	}
	return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
		"error": "Reservacion not found",
	})
}

func UpdateReservacionEditCliente(c *fiber.Ctx) error {
	fmt.Print(len(reservaciones_cliente))
	id, _ := strconv.Atoi(c.Params("id"))
	for i, reservacion := range reservaciones_cliente {
		if reservacion.IDT_Reservacion == id {
			reservacion := new(models.Reservacion_ClienteDTO)
			if err := c.BodyParser(reservacion); err != nil {
				return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
					"error": "Cannot parse JSON",
				})
			}
			reservacion.IDT_Reservacion = id
			reservaciones_cliente[i].IDT_Habitacion = reservacion.IDT_Habitacion
			return c.JSON(reservacion)
		}
	}
	return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
		"error": "Reservacion not found",
	})
}

func DeleteReservacion(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))
	for i, reservacion := range reservaciones {
		if reservacion.IDT_Reservacion == id {
			reservaciones = append(reservaciones[:i], reservaciones[i+1:]...)
			return c.SendStatus(fiber.StatusNoContent)
		}
	}
	return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
		"error": "Reservacion not found",
	})
}

func DeleteReservacionCliente(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))
	for i, reservacion := range reservaciones_cliente {
		if reservacion.IDT_Reservacion == id {
			reservaciones_cliente = append(reservaciones_cliente[:i], reservaciones_cliente[i+1:]...)
			return c.SendStatus(fiber.StatusNoContent)
		}
	}
	return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
		"error": "Reservacion not found",
	})
}
