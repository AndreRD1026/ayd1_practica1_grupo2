package rooms

import (
	roomsHandlers "main/internal/handlers/rooms"

	"github.com/gofiber/fiber/v2"
)

func SetupRoomsRoutes(router fiber.Router) {
	rooms_routers := router.Group("/rooms")
	rooms_routers.Get("/all", roomsHandlers.GetRooms)
	rooms_routers.Get("/reservadas", roomsHandlers.GetRoomsReservadas)
	rooms_routers.Post("/add", roomsHandlers.AddRoom)
	rooms_routers.Post("/reservationadd", roomsHandlers.AddReservation)

}
