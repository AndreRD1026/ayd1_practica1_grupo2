package user

import (
	userHandlers "main/internal/handlers/user"

	"github.com/gofiber/fiber/v2"
)

func SetupNoteRoutes(router fiber.Router) {
	user_router := router.Group("/user")

	//registrar usuario
	user_router.Post("/register", userHandlers.RegisterUser)
	user_router.Put("/update", userHandlers.UpdateUser)

}
