package reservaciones

import (
	reservacionesHandlers "main/internal/handlers/reservaciones"

	"github.com/gofiber/fiber/v2"
)

func SetupReservacionesRoutes(router fiber.Router) {
	reservacion_routers := router.Group("/reservaciones")

	//Rutas para reservaciones
	reservacion_routers.Get("/all", reservacionesHandlers.GetReservaciones)
	reservacion_routers.Post("/add", reservacionesHandlers.AddReservacion)
	reservacion_routers.Get("/:id", reservacionesHandlers.GetOneReservacion)
	reservacion_routers.Get("/cliente/:id", reservacionesHandlers.GetOneReservacionByCliente)
	reservacion_routers.Put("/admin/:id", reservacionesHandlers.UpdateReservacionEditAdmin)
	reservacion_routers.Put("/reservacioneseditada/:id", reservacionesHandlers.UpdateReservacionEditCliente)
	reservacion_routers.Delete("/:id", reservacionesHandlers.DeleteReservacion)
	reservacion_routers.Delete("/reservacionescliente/:id", reservacionesHandlers.DeleteReservacionCliente)

}
