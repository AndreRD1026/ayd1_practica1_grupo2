package routes

import (
	"main/internal/routes/reservaciones"
	"main/internal/routes/rooms"
	"main/internal/routes/user"

	"github.com/gofiber/fiber/v2"
)

func SetupRoutes(app *fiber.App) {
	//Endpoint Base
	app.Get("/", func(c *fiber.Ctx) error {
		response := struct {
			Grupo       string `json:"name"`
			Integrantes string `json:"website"`
		}{
			Grupo:       "Análisis y diseño 1 - Grupo 2",
			Integrantes: "César André Ramírez Dávila - 202010816,  Angel Francisco Sique Santos - 202012039 , Kevin Gerardo Ruiz Yupe - 201903791 , Omar Alejandro Vides Esteban - 201709146, Heinz Ricardo Gómez Galindo - 199819617",
		}
		return c.JSON(response)
	})
	// Setup user routes
	reservaciones.SetupReservacionesRoutes(app)
	rooms.SetupRoomsRoutes(app)
	user.SetupNoteRoutes(app)

}
