package models

type Reservacion_ClienteDTO struct {
	IDT_Reservacion    int    `json:"idt_reservacion"`
	IDT_Cliente        int    `json:"idt_cliente"`
	IDT_Habitacion     int    `json:"idt_habitacion"`
	Fecha_Ini          string `json:"fecha_entrada"`
	Fecha_Fin          string `json:"fecha_salida"`
	Estado_Reservacion int    `json:"estado_reservacion"`
}

type ReservacionDTO struct {
	IDT_Reservacion    int    `json:"idt_reservacion"`
	IDT_Cliente        int    `json:"idt_cliente"`
	IDT_Habitacion     int    `json:"idt_habitacion"`
	Fecha_Ini          string `json:"fecha_ini"`
	Fecha_Fin          string `json:"fecha_fin"`
	Estado_Reservacion int    `json:"estado_reservacion"`
}
