package models

type UserDTO struct {
	Id_cliente   int `json:"id_cliente"`
	Id_persona   int `json:"id_persona"`
	Id_usuario   int `json:"id_usuario"`
	Id_documento int `json:"id_documento"`
	Id_rol       int `json:"id_rol"`

	Descripcion_doc   string `json:"descripcion_doc"`
	Descripcion_rol   string `json:"descripcion_rol"`
	Nombres           string `json:"nombres"`
	Usuario           string `json:"usuario"`
	Contrasena        string `json:"contrasena"`
	Estado            int    `json:"estado"`
	No_identificacion int    `json:"no_identificacion"`
	Numero_tel        int    `json:"numero_tel"`
}
