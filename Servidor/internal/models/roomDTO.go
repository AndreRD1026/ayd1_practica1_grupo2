package models

type RoomDTO struct {
	IDT_Habitacion   int    `json:"idt_habitacion"`
	NumeroHabitacion int    `json:"numerohabitacion"`
	Nombre           string `json:"nombre"`
	Descripcion      string `json:"descripcion"`
	Estado           int    `json:"estado"`
}
