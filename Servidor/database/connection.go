package database

import (
	"database/sql"
	"fmt"
	"main/config"

	_ "github.com/go-sql-driver/mysql"
)

// Database instance
var DB *sql.DB

// Connect Function
func Connect() error {
	var err error
	//get port from .env
	//dsn
	DB, err = sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", config.Config("DB_USER"), config.Config("DB_PASSWORD"), config.Config("DB_HOST"), config.Config("DB_PORT"), config.Config("DB_NAME")))
	if err != nil {
		return err
	}
	if err = DB.Ping(); err != nil {
		return err
	}
	return nil
}
