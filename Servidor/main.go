package main

import (
	"log"
	"main/database"
	"main/internal/routes"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func main() {
	// Connect with database
	if err := database.Connect(); err != nil {
		log.Fatal(err)
	}
	app := fiber.New()
	app.Use(cors.New())
	routes.SetupRoutes(app)
	app.Listen(":3002")
}
